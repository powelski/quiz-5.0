<?php

return [
    'thresholds' => [
        'cel'   => 1,
        'bdb'   => 0.91,
        'db+'   => 0.81,
        'db'    => 0.71,
        'dst+'  => 0.61,
        'dst'   => 0.51,
        'ndst'  => 0
    ],

    'below' => 'ndst!',

    'passed' => 'zal'
];
