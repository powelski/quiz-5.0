<?php

use Illuminate\Database\Seeder;

class SampleDataSeeder extends Seeder {

    private function getData()
    {
        return [
            'Quiz' => [
                [
                    'title' => 'Przykładowy test',
                    'description' => 'Test bez większego sensu.'
                ],
                [
                    'title' => 'PHP',
                    'description' => 'Podstawowe funkcje i zastosowanie języka PHP.'
                ],
                [
                    'title' => 'JavaScript',
                    'description' => 'Podstawowe funkcje i zastosowanie języka JavaScript.'
                ]
            ],
            'Question' => [
                [
                    'quiz_id' => 1,
                    'content' => 'Ile wynosi wynik sumy 1 i 2?',
                    'choice'  => 'single'
                ],
                [
                    'quiz_id' => 1,
                    'content' => 'Które kolory znajdują się na fladze Polski?',
                    'choice'  => 'multiple'
                ],
                [
                    'quiz_id' => 1,
                    'content' => 'Co wisi u mnie w domu na choince?',
                    'choice'  => 'single'
                ]
            ]
        ];
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $model => $data) {
            $class = 'Quiz\\' . $model;
            foreach ($data as $row) {
                $class::create($row);
            }
        }
    }

}
