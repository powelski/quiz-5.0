/*jslint browser: true*/
(function (init) {
    "use strict";

    init(window, window.jQuery, window.moment);
}(function (window, $, moment) {
    "use strict";

    var Config = {},
        View = {},
        Api = {};

    Config = {
        transition: {
            in: {
                opacity: 1
            },
            out: {
                opacity: 0
            },
            duration: 200
        }
    };

    View = {
        current: "",

        show: function (id, args) {
            var self = {},
                $view = {};

            self = this;

            $view = $(".view").filter(function () {
                return $(this).data("view") === id;
            });

            if ($view.length !== 1) {
                throw "View " + id + " not found.";
            }

            if (self.current !== "") {
                $(".view")
                    .filter(function () {
                        return $(this).data("view") === self.current;
                    })
                    .stop()
                    .animate(
                        Config.transition.out,
                        Config.transition.duration,
                        function () {
                            var cleanCallback = {};

                            $(this).hide();

                            cleanCallback = View.pages[$(this).data("view")].clean;
                            if ($.isFunction(cleanCallback)) {
                                cleanCallback.call(this);
                            }
                        }
                    );
            }

            if (!$.isArray(args)) {
                args = [];
            }

            if (this.pages.hasOwnProperty(id) && $.isFunction(this.pages[id].prepare)) {
                this.pages[id].prepare.apply($view.get(0), args);
            }

            if (self.current !== "") {
                $view
                    .stop()
                    .css(Config.transition.out)
                    .show()
                    .animate(Config.transition.in, Config.transition.duration);
            } else {
                $view.show();
            }

            this.current = id;

            if (this.pages.hasOwnProperty(id) && $.isFunction(this.pages[id].show)) {
                this.pages[id].show.apply($view.get(0), args);
            }
        },

        pages: {
            guest: {
                init: function () {
                    var $self = {};

                    $self = $(this);

                    $(this).find("input").keydown(function (e) {
                        if (e.which === 13) {
                            $self.find(".log-in").click();
                        }
                    });

                    $(this).find(".log-in").click(function () {
                        var $email    = {},
                            $password = {};

                        try {
                            $email    = $self.find(".email");
                            $password = $self.find(".password");

                            if ($email.val() === "") {
                                throw $email;
                            }

                            if (!(new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")).test($email.val())) {
                                throw $email;
                            }

                            if ($password.val() === "") {
                                throw $password;
                            }
                        } catch (field) {
                            $(field).select();
                            return true;
                        }

                        $self.addClass("loading");

                        Api.call(
                            "login?with=me,quizzes",
                            "POST",
                            {
                                email:    $self.find(".email").val(),
                                password: $self.find(".password").val()
                            },
                            function (response) {
                                localStorage.setItem("api_key", response.key);
                                View.show("quizzes", [response.quizzes]);
                                $(".my-first-name").text(response.me.first_name);
                                $(".my-last-name").text(response.me.last_name);
                            },
                            function (jqXHR) {
                                if (jqXHR.status === 400) {
                                    $self.find("input[type=email]").select();
                                } else if (jqXHR.status === 401) {
                                    $self.find("input[type=password]").select();
                                }
                            },
                            function () {
                                $self.removeClass("loading");
                            }
                        );
                    });

                    $(this).find(".register").click(function () {
                        View.show("register");
                    });
                },

                show: function () {
                    localStorage.removeItem("api_key");

                    $(this).find("input[type=email]").select();
                    $(".log-out").addClass("hidden");
                    $(".my-first-name, .my-last-name").empty();
                },

                clean: function () {
                    $(this).find("input[type=email], input[type=password]").val("");
                },
            },

            home: {
                prepare: function () {
                    $(this).addClass("loading");

                    Api.call(
                        "init?with=me,quizzes",
                        "GET",
                        {
                            key: localStorage.getItem("api_key")
                        },
                        function (response) {
                            View.show("quizzes", [response.quizzes]);
                            $(".my-first-name").text(response.me.first_name);
                            $(".my-last-name").text(response.me.last_name);
                        },
                        function (jqXHR) {
                            if (jqXHR.status === 403) {
                                View.show("guest");
                            }
                        }
                    );
                },

                show: function () {
                    $(".log-out").addClass("hidden");
                    $(".my-first-name, .my-last-name").empty();
                },

                clean: function () {
                    $(this).removeClass("loading");
                }
            },

            register: {
                init: function () {
                    var $self = {};

                    $self = $(this);

                    $(this).find(".register").click(function () {
                        var $email    = {},
                            $password = {},
                            $firstName = {},
                            $lastName = {};

                        try {
                            $email    = $self.find(".email");
                            $password = $self.find(".password");
                            $firstName = $self.find(".first-name");
                            $lastName = $self.find(".last-name");

                            if ($email.val() === "") {
                                throw $email;
                            }

                            if (!(new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")).test($email.val())) {
                                throw $email;
                            }

                            if ($password.val() === "") {
                                throw $password;
                            }

                            if ($firstName.val() === "") {
                                throw $firstName;
                            }

                            if ($lastName.val() === "") {
                                throw $lastName;
                            }
                        } catch (field) {
                            $(field).select();
                            return true;
                        }

                        $self.addClass("loading");

                        Api.call(
                            "register?with=me,quizzes",
                            "POST",
                            {
                                email:     $email.val(),
                                password:  $password.val(),
                                firstName: $firstName.val(),
                                lastName:  $lastName.val()
                            },
                            function (response) {
                                localStorage.setItem("api_key", response.key);
                                View.show("quizzes", [response.quizzes]);
                                $(".my-first-name").text(response.me.first_name);
                                $(".my-last-name").text(response.me.last_name);
                            },
                            undefined,
                            function () {
                                $self.removeClass("loading");
                            }
                        );
                    });

                    $(this).find(".back").click(function () {
                        View.show("guest");
                    });

                    $(this).find("input").keydown(function (e) {
                        if (e.which === 13) {
                            $self.find(".register").click();
                        }
                    });
                },

                show: function () {
                    $(this).find(".email").select();
                    $(".log-out").addClass("hidden");
                    $(".my-first-name, .my-last-name").empty();
                },

                clean: function () {
                    $(this).find(".email, .password, .first-name, .last-name").val("");
                },

                keys: {
                    backBackspace: {
                        keys:   [8],
                        action: function () {
                            $(this).find(".back").click();
                        },
                        interrupt: false
                    },
                    backEscape: {
                        keys:   [27],
                        action: function () {
                            $(this).find(".back").click();
                        },
                        interrupt: true
                    }
                }
            },

            quizzes: {
                init: function () {
                    $(this).on("click", ".quiz", function () {
                        View.show("intro", [$(this).data("quiz")]);
                    });
                },

                /**
                 *  @param {object|boolean} refresh
                 */
                prepare: function (quizzes) {
                    if (quizzes !== undefined) {
                        $(this)
                            .find(".quizzes")
                            .empty()
                            .addClass("faded-out")
                            .append(
                                $.map(quizzes, function (quiz) {
                                    return $("<a></a>")
                                        .data("quiz", quiz)
                                        .addClass("quiz option relative")
                                        .append([
                                            $("<div></div>").addClass("quiz-icon").css({
                                                backgroundColor: quiz.hasOwnProperty("icon_background") ? "#" + quiz.icon_background : undefined,
                                                backgroundImage: quiz.hasOwnProperty("icon_file") ? "url(" + quiz.icon_file + ")" : undefined
                                            }),
                                            $("<div></div>").addClass("quiz-details").append(
                                                $("<div></div>").addClass("title").text(quiz.title)
                                            ),
                                            quiz.hasOwnProperty("grade")
                                                ? $("<div></div>").addClass("grade").text(quiz.grade)
                                                : null
                                        ]);
                                })
                            )
                            .removeClass("faded-out");
                    }
                },

                show: function () {
                    $(".log-out").removeClass("hidden");
                }
            },

            intro: {
                init: function () {
                    $(this).find(".back").click(function () {
                        View.show("quizzes");
                    });

                    $(this).find(".take-quiz, .see-results").click(function () {
                        View.show("quiz", [$(this).data("quiz")]);
                    });
                },

                prepare: function (quiz) {
                    $(this).find(".quiz-icon").css({
                        backgroundColor: quiz.hasOwnProperty("icon_background") ? "#" + quiz.icon_background : undefined,
                        backgroundImage: quiz.hasOwnProperty("icon_file") ? "url(" + quiz.icon_file + ")" : undefined
                    });

                    $(this).find(".title").text(quiz.title);
                    $(this).find(".description").text(quiz.description);

                    if (quiz.hasOwnProperty("finished_at")) {
                        $(this).find(".see-results").data("quiz", quiz);
                        $(this).find(".take-quiz").hide();
                        $(this).find(".quiz-finished-at").text(moment(quiz.finished_at).fromNow());

                        if (quiz.hasOwnProperty("score")) {
                            $(".quiz-score").text((quiz.score * 100).toFixed(2));
                        } else {
                            $(".quiz-results-score").hide();
                        }
                    } else {
                        $(this).find(".take-quiz").data("quiz", quiz);
                        $(this).find(".quiz-results, .see-results").hide();
                    }
                },

                show: function () {
                    $(".log-out").removeClass("hidden");
                },

                clean: function () {
                    $(this).find(".quiz-icon").css({
                        backgroundColor: "",
                        backgroundImage: ""
                    });

                    $(this).find(".take-quiz, .see-results, .quiz-results, .quiz-results-score").show();
                    $(this).find(".quiz-finished-at, .quiz-score").empty();
                },

                keys: {
                    back: {
                        keys:   [8, 27],
                        action: function () {
                            $(this).find(".back").click();
                        }
                    }
                }
            },

            quiz: {
                init: function () {
                    var $self = {};

                    $self = $(this);

                    $(this).on("click", ".radio:not(.freezed)", function () {
                        $(this)
                            .toggleClass("checked")
                            .siblings(".radio")
                            .removeClass("checked");
                    });

                    $(this).on("click", ".checkbox:not(.freezed)", function () {
                        $(this).toggleClass("checked");
                    });

                    $(this).find(".back").click(function () {
                        View.show("quizzes");
                    });

                    $(this).find(".previous").click(function () {
                        var $current = {},
                            $previous = {};

                        if ($(this).hasClass("disabled")) {
                            return true;
                        }

                        $current = $self.find(".current-question");
                        $previous = $current.prev();

                        $self.find(".next").removeClass("disabled");

                        $current
                            .removeClass("current-question")
                            .stop()
                            .fadeOut(Config.transition.duration);

                        $previous
                            .addClass("current-question")
                            .stop()
                            .fadeIn(Config.transition.duration);

                        if ($previous.is(":first-child")) {
                            $(this).addClass("disabled");
                        }

                        $self.find(".finish").addClass("faded-out");
                    });

                    $(this).find(".next").click(function () {
                        var $current = {},
                            $next = {};

                        if ($(this).hasClass("disabled")) {
                            return true;
                        }

                        $current = $self.find(".current-question");
                        $next = $current.next();

                        $self.find(".previous").removeClass("disabled");

                        $current
                            .removeClass("current-question")
                            .stop()
                            .fadeOut(Config.transition.duration);

                        $next
                            .addClass("current-question")
                            .stop()
                            .fadeIn(Config.transition.duration);

                        if ($next.is(":last-child")) {
                            $(this).addClass("disabled");
                            $self.find(".finish").removeClass("faded-out");
                        } else {
                            $self.find(".finish").addClass("faded-out");
                        }
                    });

                    $(".finish").click(function () {
                        var answers = [],
                            quizId = 0;

                        $self.addClass("loading");

                        $self.find(".questions").fadeOut(Config.transition.duration);
                        $self.find(".sending-answers").fadeIn(Config.transition.duration);

                        quizId = $(this).data("id");

                        answers = $self.find(".answer.checked").map(function () {
                            return $(this).data("id");
                        }).get();

                        if (answers.length === 0) {
                            answers = "none";
                        }

                        Api.call(
                            "quiz/" + quizId + "?with=quizzes",
                            "POST",
                            {
                                answers:  answers,
                                key:      localStorage.getItem("api_key")
                            },
                            function (result) {
                                View.show("quizzes", [result.quizzes]);
                            },
                            function () {
                                $self.find(".sending-answers").fadeOut(Config.transition.duration);
                                $self.find(".questions").fadeIn(Config.transition.duration);
                            },
                            function () {
                                $self.removeClass("loading");
                            }
                        );
                    });
                },

                prepare: function (quiz) {
                    var $self;

                    $self = $(this);

                    $self.addClass("loading");

                    $self.find(".quiz-icon").css({
                        backgroundColor: quiz.hasOwnProperty("icon_background") ? "#" + quiz.icon_background : undefined,
                        backgroundImage: quiz.hasOwnProperty("icon_file") ? "url(" + quiz.icon_file + ")" : undefined
                    });

                    $self.find(".title").text(quiz.title);
                    $self.find(".description").text(quiz.description);
                    $self.find(".sending-answers").hide();

                    $self.find(".previous, .next").addClass("disabled");
                    $self.find(".finish").addClass(quiz.hasOwnProperty("finished_at") ? "hidden" : "faded-out");

                    Api.call(
                        "quiz/" + quiz.id,
                        "POST",
                        {
                            key: localStorage.getItem("api_key")
                        },
                        function (quiz) {
                            $self.find(".title").text(quiz.title);
                            $self.find(".description").text(quiz.description);
                            $self.find(".loading-questions").fadeOut(Config.transition.duration);

                            $self.find(".finish").data("id", quiz.id);

                            $self
                                .find(".questions")
                                .append(
                                    $.map(quiz.questions, function (question) {
                                        return $("<div></div>")
                                            .addClass("question-wrapper left-top hidden")
                                            .append([
                                                $("<div></div>").addClass("question").html(question.content),
                                                $("<div></div>").append(
                                                    $.map(question.answers, function (answer, index) {
                                                        return $("<a></a>")
                                                            .addClass("answer option relative")
                                                            .addClass(question.choice === "single" ? "radio" : "checkbox")
                                                            .toggleClass("solved freezed", quiz.hasOwnProperty("finished_at"))
                                                            .toggleClass("checked", answer.hasOwnProperty("selected") && answer.selected)
                                                            .addClass(answer.status)
                                                            .data("id", answer.id)
                                                            .append(
                                                                $("<div></div>").addClass("key-hint").text(index + 1),
                                                                answer.content
                                                            );
                                                    })
                                                ),
                                                question.hasOwnProperty("hint") || question.choice === "multiple"
                                                    ? $("<div></div>")
                                                        .addClass("hint")
                                                        .append(question.choice === "multiple" ? "To jest pytanie wielokrotnego wyboru. " : null)
                                                        .append(question.hint)
                                                    : null
                                            ]);
                                    })
                                );

                            $self
                                .find(".question-wrapper")
                                .first()
                                .addClass("current-question")
                                .fadeIn(Config.transition.duration);

                            if (quiz.questions.length <= 1) {
                                $self.find(".finish").removeClass("faded-out");
                            } else {
                                $self.find(".next").removeClass("disabled");
                            }
                        },
                        function () {
                            View.show("quizzes");
                        },
                        function () {
                            $self.removeClass("loading");
                        }
                    );
                },

                show: function () {
                    $(".log-out").removeClass("hidden");
                },

                clean: function () {
                    $(this).find(".title, .description, .questions").empty();
                    $(this).find(".questions, .loading-questions, .sending-answers").show();

                    $(this).find(".quiz-icon").css({
                        backgroundColor: "",
                        backgroundImage: ""
                    });

                    $(this).find(".finish")
                        .removeData("id")
                        .removeClass("faded-out");
                },

                keys: {
                    answer: {
                        keys:   [49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59],
                        action: function (key) {
                            $(this).find(".answer:visible").eq(key - 49).click();
                        }
                    },
                    previous: {
                        keys:   [8, 37, 38],
                        action: function () {
                            $(this).find(".previous").click();
                        }
                    },
                    next: {
                        keys:   [13, 39, 40],
                        action: function () {
                            $(this).find(".next").click();
                        }
                    },
                    back: {
                        keys:   [8, 27],
                        action: function () {
                            if ($(this).find(".solved").length > 0) {
                                $(this).find(".back").click();
                            }
                        }
                    }
                }
            }
        }
    };

    Api = {
        init: function (url) {
            this.url = url.replace(/\/+$/g, "");
        },

        call: function (uri, method, data, success, error, complete) {
            if (method === undefined) {
                method = "GET";
            }

            $.ajax({
                url:      this.url + "/" + uri,
                method:   method,
                data:     data,
                success:  success,
                error:    error,
                complete: complete,
                dataType: "json"
            });
        }
    };

    $(function () {
        Api.init(window.Quiz.routes.api);

        $(".view").each(function () {
            if (!View.pages.hasOwnProperty($(this).data("view")) || !$.isFunction(View.pages[$(this).data("view")].init)) {
                return true;
            }

            View.pages[$(this).data("view")].init.call(this);
        });

        $(".log-out").click(function () {
            View.show("guest");

            Api.call(
                "logout",
                "POST",
                {
                    key: localStorage.getItem("api_key")
                }
            );

            localStorage.removeItem("api_key");
        });

        $(document).keydown(function (e) {
            var result = true,
                view = {},
                $view = {};

            if (!View.pages.hasOwnProperty(View.current)) {
                return true;
            }

            view = View.pages[View.current];

            if (!view.hasOwnProperty("keys")) {
                return true;
            }

            $view = $(".view").filter(function () {
                return $(this).data("view") === View.current;
            });

            $.each(view.keys, function (name, entry) {
                /*jslint unparam: true*/
                var interrupt = false;

                if (entry.hasOwnProperty("interrupt")) {
                    interrupt = entry.interrupt;
                }

                if (entry.keys.indexOf(e.which) >= 0 && (interrupt || !$(e.target).is("input, textarea"))) {
                    entry.action.call($view, e.which);
                    result = false;
                }
            });

            return result;
        });

        View.show(localStorage.getItem("api_key") !== null ? "home" : "guest");
    });
}));
