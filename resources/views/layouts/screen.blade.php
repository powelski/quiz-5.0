<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Przetestuj się!</title>
		{!! Html::style('css/quiz.css') !!}
		{!! Html::style('http://fonts.googleapis.com/css?family=Titillium+Web|Shadows+Into+Light+Two') !!}
	</head>
	<body>
		<div class="content">
			<div class="views relative box">
				<div class="view left-top" data-view="guest">
					<div class="full-wrapper">
						<div class="full-content">
							<div class="authentication-forms">
								<div class="log-in-form">
									<header class="main-header">Mam już konto</header>
									<input type="email" class="email" placeholder="Adres e-mail">
									<input type="password" class="password" placeholder="Hasło">
									<a class="log-in button button-success">Zaloguj mnie</a>
								</div>
								<div class="register-block">
									<header class="main-header">Chcę się przyłączyć</header>
									<a class="register button">Zarejestruj mnie</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="view left-top" data-view="home"></div>

				<div class="view left-top" data-view="register">
					<div class="full-wrapper">
						<div class="full-content">
							<div class="register-form">
								<header class="main-header">Rejestracja</header>
								<input type="email" class="email" placeholder="Adres e-mail">
								<input type="password" class="password" placeholder="Hasło">
								<input type="text" class="first-name" placeholder="Imię">
								<input type="text" class="last-name" placeholder="Nazwisko">
								<a class="register button button-success">Zapisz mnie</a>
							</div>
						</div>
					</div>
					<footer>
						<a class="back button">Powrót</a>
					</footer>
				</div>

				<div class="view left-top" data-view="quizzes">
					<header class="main-header">Moje testy</header>
					<div class="quizzes fading"></div>
				</div>

				<div class="view left-top table" data-view="intro">
					<div class="full-wrapper">
						<div class="intro-content full-content">
							<div class="quiz-icon"></div>
							<div class="title"></div>
							<p class="description"></p>
							<a class="take-quiz button button-success">Rozpocznij test</a>
							<div class="quiz-results">
								<div class="quiz-results-time">
									Test został rozwiązany <time class="quiz-finished-at"></time>
								</div>
								<div class="quiz-results-score">
									z wynikiem <div class="quiz-score"></div>
								</div>
							</div>
							<a class="see-results button">Moje odpowiedzi</a>
						</div>
					</div>
					<footer>
						<a class="back button">Powrót</a>
					</footer>
				</div>

				<div class="view left-top" data-view="quiz">
					<div class="full-wrapper">
						<div class="full-content">
							<div class="loading-questions message-loading">Zaczekaj chwilę...</div>
							<div class="sending-answers message-loading">Oceniam test...</div>
						</div>
					</div>
					<header class="quiz-header">
						<div class="quiz-icon"></div>
						<div class="quiz-info">
							<div class="title"></div>
							<p class="description"></p>
						</div>
					</header>
					<div class="questions relative"></div>
					<footer>
						<div class="left">
							<a class="back button">Powrót</a>
						</div>
						<div class="right">
							<a class="finish button button-success fading">Zakończ test</a>
							<a class="previous button">Poprzednie</a>
							<a class="next button">Następne</a>
						</div>
					</footer>
				</div>
				<div class="me">
					<div class="my-first-name"></div>
					<div class="my-last-name"></div>
				</div>
				<a class="log-out"></a>
			</div>
		</div>
		@include('scripts')
		{!! Html::script('js/bower_components/jQuery/dist/jquery.min.js') !!}
		{!! Html::script('js/bower_components/moment/min/moment.min.js') !!}
		{!! Html::script('js/bower_components/moment/locale/pl.js') !!}
		{!! Html::script('js/quiz.js') !!}
	</body>
</html>