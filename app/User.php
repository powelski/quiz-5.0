<?php namespace Quiz;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use \DB;
use Quiz\Quiz;
use Quiz\Answer;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function quizzes()
    {
        return $this
            ->belongsToMany('Quiz\Quiz')
            ->wherePivot('available_at', '<=', DB::raw('NOW()'))
            ->withTimestamps();
    }

    public function takenQuizzes()
    {
        return $this
            ->belongsToMany('Quiz\Quiz', 'quiz_user_takes')
            ->withPivot('score', 'started_at', 'finished_at')
            ->withTimestamps();
    }

    public function answers()
    {
        return $this->belongsToMany('Quiz\Answer')->withTimestamps();
    }

    public function groups()
    {
        return $this->belongsToMany('Quiz\Group')->withTimestamps();
    }

    public function permissions()
    {
        return $this->belongsToMany('Quiz\Permission')->withTimestamps();
    }

    public function countScore(Quiz $quiz)
    {
        $attendance = $this->takenQuizzes()->find($quiz->id);

        if ($attendance === null || $attendance->finished_at === null) {
            return false;
        }

        $questions = $quiz
            ->questions()
            ->with([
                'answers' => function ($query) {
                    $query
                        ->select(
                            'answers.question_id',
                            'answers.points',
                            'answer_user.user_id'
                        )
                        ->leftJoin('answer_user', function ($query) {
                            $query
                                ->on('answer_user.answer_id', '=', 'answers.id')
                                ->on('answer_user.user_id', '=', DB::raw($this->id));
                        });
                }
            ])
            ->get(['id', 'choice', 'content']);

        if ($questions->count() === 0) {
            return 1;
        }

        $scores = $questions
            ->map(function ($question) {
                $points = $question
                    ->answers
                    ->filter(function ($answer) {
                        return $answer->user_id !== null;
                    })
                    ->sum('points');

                $checkedCount = $question
                    ->answers
                    ->filter(function ($answer) {
                        return $answer->user_id !== null;
                    })
                    ->count();

                $validCount = $question
                    ->answers
                    ->filter(function ($answer) {
                        return $answer->points > 0;
                    })
                    ->count();

                if ($checkedCount === 0 && $validCount === 0) {
                    return 1;
                }

                if ($question->choice === 'single') {
                    $min = $question
                        ->answers
                        ->min('points');

                    $max = $question
                        ->answers
                        ->max('points');
                } else {
                    $min = $question
                        ->answers
                        ->filter(function ($answer) {
                            return $answer->points < 0;
                        })
                        ->sum('points');

                    $max = $question
                        ->answers
                        ->filter(function ($answer) {
                            return $answer->points > 0;
                        })
                        ->sum('points');
                }

                if ($min > 0) {
                    $min = 0;
                }

                if ($max < 0) {
                    $max = 0;
                }

                if ($min === $max) {
                    return 1;
                }

                return 2 * ($points - $min) / ($max - $min) - 1;
            });

        return $scores->sum() / $questions->count();
    }

    public function getGrade(Quiz $quiz)
    {
        $attendance = $this->takenQuizzes()->find($quiz->id);

        if ($attendance === null || $attendance->finished_at === null) {
            return false;
        }

        if (!$quiz->isGradable()) {
            return config('grades.passed');
        }

        $grades = config('grades.thresholds');

        arsort($grades);

        foreach ($grades as $grade => $threshold) {
            if ($attendance->score >= $threshold) {
                return $grade;
            }
        }

        return config('grades.below');
    }
}
