<?php namespace Quiz;

use Illuminate\Database\Eloquent\Model;
use Quiz\User;
use \DB;

class Quiz extends Model
{
    public function questions()
    {
        return $this->hasMany('Quiz\Question');
    }

    public function answers()
    {
        return $this->hasManyThrough('Quiz\Answer', 'Quiz\Question');
    }

    public function users()
    {
        return $this
            ->belongsToMany('Quiz\User')
            ->wherePivot('available_at', '<=', DB::raw('NOW()'))
            ->withTimestamps();
    }

    public function groups()
    {
        return $this
            ->belongsToMany('Quiz\Group')
            ->wherePivot('available_at', '<=', DB::raw('NOW()'))
            ->withTimestamps();
    }

    public function takers()
    {
        return $this
            ->belongsToMany('Quiz\User', 'quiz_user_takes')
            ->withPivot('score', 'started_at', 'finished_at')
            ->withTimestamps();
    }

    public function scopeAvailable($query, User $user)
    {

        $userPermissions = $user->permissions()->lists('key');

        $viewAllPermissions = [
            'update_quiz'
        ];

        if (count(array_intersect($userPermissions, $viewAllPermissions)) > 0) {
            return $query;
        }

        $ids = [];

        $ids = array_merge(
            $ids,
            $user
                ->quizzes()
                ->select('quizzes.id')
                ->lists('id')
        );

        foreach ($user->groups()->get() as $group) {
            $ids = array_merge(
                $ids,
                $group
                    ->quizzes()
                    ->select('quizzes.id')
                    ->lists('id')
            );
        }

        $ids = array_merge(
            $ids,
            Quiz::whereAccess('public')
                ->select('quizzes.id')
                ->lists('id')
        );

        $ids = array_values(array_unique($ids));

        return $query->whereIn('id', $ids);
    }

    public function getIconFileAttribute($value)
    {
        return asset('images/quizzes/' . $value);
    }

    public function isGradable()
    {
        return $this
            ->answers()
            ->where('points', '!=', 0)
            ->count() > 0;
    }
}
