<?php namespace Quiz\Http\Controllers;

use JavaScript;

class HomeController extends Controller
{
    public function index()
    {
        JavaScript::put([
            'routes' => [
                'api' => url(config('api.uri'))
            ]
        ]);

        return view('layouts.screen');
    }
}
