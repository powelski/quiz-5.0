<?php namespace Quiz\Http\Controllers;

use Quiz\User;
use Quiz\Quiz;
use Quiz\Answer;
use Quiz\Key;
use Quiz\Group;
use \DB;
use \Input;
use \Exception;
use \Auth;
use \Hash;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiController extends Controller
{
    private function attachData(array $keys, User $user)
    {
        $map = [
            'me' => function (User $user) {
                return $this->userName($user);
            },
            'quizzes' => function (User $user) {
                return $this->userQuizzes($user);
            }
        ];

        $result = [];

        $keys = array_values(array_unique($keys));

        $invalidKeys  = array_diff($keys, array_keys($map));

        if (count($invalidKeys) > 0) {
            $message = 'Unknown with parameter value';

            if (count($invalidKeys) !== 1) {
                $message .= 's';
            }

            $message .= ': ' . implode(', ', $invalidKeys) . '.';

            throw new HttpException(400, $message);
        }

        foreach ($keys as $key) {
            $result[$key] = $map[$key]($user);
        }

        return $result;
    }

    private function userName(User $user)
    {
        return [
            'first_name' => $user->first_name,
            'last_name'  => $user->last_name
        ];
    }

    private function userQuizzes(User $user)
    {
        $quizzes = Quiz::available($user)
            ->orderBy('created_at')
            ->get([
                'id',
                'title',
                'description',
                'icon_file',
                'icon_background'
            ])
            ->map(function ($quiz) use ($user) {
                $take = $quiz
                    ->takers()
                    ->find(
                        $user->id,
                        [
                            'score',
                            'started_at',
                            'finished_at'
                        ]
                    );

                if ($take !== null) {
                    $quiz->started_at = $take->started_at;

                    if ($take->finished_at !== null) {
                        $quiz->finished_at = $take->finished_at;

                        if ($quiz->isGradable()) {
                            if ($take->score !== null) {
                                $quiz->score = (float) $take->score;
                            }
                        }

                        $quiz->grade = $user->getGrade($quiz);
                    }
                }

                $quiz = array_filter(
                    $quiz->toArray(),
                    function ($value) {
                        return $value !== null;
                    }
                );

                return $quiz;
            });

        return $quizzes;
    }

    public function postLogin()
    {
        try {
            foreach (['email', 'password'] as $parameter) {
                if (!Input::has($parameter)) {
                    throw new HttpException(400, 'Missing ' . $parameter . ' parameter.');
                }
            }

            if (!Auth::validate(Input::only('email', 'password'))) {
                throw new AccessDeniedHttpException('Login attempt failed.');
            }

            $me = User::whereEmail(Input::get('email'))->firstOrFail();

            do {
                $random = Str::lower(str_random(32));
            } while (Key::whereKey($random)->count() > 0);

            $key = new Key;
            $key->user()->associate($me);
            $key->key = $random;
            $key->save();

            $response = [];

            if (Input::has('with')) {
                $response += $this->attachData(explode(',', Input::get('with')), $me);
            }

            $response['key'] = $key->key;

            $status = 200;
        } catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status = $exception->getStatusCode();
        } catch (Exception $exception) {
            $response = config('app.debug') ? $exception->getMessage() : 'Internal error occurred.';
            $status = 500;
        }

        return response()->json($response, $status);
    }

    public function postLogout()
    {
        try {
            if (!Input::has('key')) {
                throw new AccessDeniedHttpException('Missing API key.');
            }

            $key = Key::whereKey(Input::get('key'))->first();

            if ($key === null) {
                throw new AccessDeniedHttpException('Invalid API key.');
            }

            $key->delete();

            $response = [
                'success' => true
            ];

            $status = 200;
        } catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status = $exception->getStatusCode();
        } catch (Exception $exception) {
            $response = config('app.debug') ? $exception->getMessage() : 'Internal error occurred.';
            $status = 500;
        }

        return response()->json($response, $status);
    }

    public function postRegister()
    {
        try {
            foreach (['email', 'password', 'firstName', 'lastName'] as $parameter) {
                if (!Input::has($parameter)) {
                    throw new HttpException(400, 'Missing ' . $parameter . ' parameter.');
                }
            }

            extract(Input::only('email', 'password', 'firstName', 'lastName'));

            if (User::whereEmail($email)->count() > 0) {
                throw new AccessDeniedHttpException('E-mail is already registered.');
            }

            if (Str::length($password) < 7) {
                throw new AccessDeniedHttpException('Password must be at least 7 characters long.');
            }

            $me = new User();
            $me->email = $email;
            $me->password = Hash::make($password);
            $me->first_name = $firstName;
            $me->last_name = $lastName;
            $me->save();

            do {
                $random = Str::lower(str_random(32));
            } while (Key::whereKey($random)->count() > 0);

            $key = new Key;
            $key->user()->associate($me);
            $key->key = $random;
            $key->save();

            $response = [];

            if (Input::has('with')) {
                $response += $this->attachData(explode(',', Input::get('with')), $me);
            }

            $response['key'] = $key->key;

            $status = 200;
        } catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status = $exception->getStatusCode();
        } catch (Exception $exception) {
            $response = config('app.debug') ? $exception->getMessage() : 'Internal error occurred.';
            $status = 500;
        }

        return response()->json($response, $status);
    }

    public function getInit()
    {
        try {
            if (!Input::has('key')) {
                throw new AccessDeniedHttpException('Missing API key.');
            }

            $key = Key::whereKey(Input::get('key'))->first();

            if ($key === null) {
                throw new AccessDeniedHttpException('Invalid API key.');
            }

            $me = $key->user()->firstOrFail();

            $response = [];

            if (Input::has('with')) {
                $response += $this->attachData(explode(',', Input::get('with')), $me);
            }

            $status = 200;
        } catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status = $exception->getStatusCode();
        } catch (Exception $exception) {
            $response = config('app.debug') ? $exception->getMessage() : 'Internal error occurred.';
            $status = 500;
        }

        return response()->json($response, $status);
    }

    public function postQuiz($id)
    {
        try {
            $quiz = Quiz::find($id);

            if ($quiz === null) {
                throw new NotFoundHttpException('Quiz not found.');
            }

            if (!Input::has('key')) {
                throw new AccessDeniedHttpException('Missing API key.');
            }

            $key = Key::whereKey(Input::get('key'))->first();

            if ($key === null) {
                throw new AccessDeniedHttpException('Invalid API key.');
            }

            $me = $key->user()->firstOrFail();

            if (Quiz::available($me)->find($id) === null) {
                throw new AccessDeniedHttpException('Access denied.');
            }

            if (Input::has('answers')) {
                $answersIds = Input::get('answers');

                if ($answersIds === 'none') {
                    $answersIds = [];
                }

                if (!is_array($answersIds)) {
                    throw new HttpException(400, 'Invalid answers parameter.');
                }

                if (count($answersIds) !== count(array_unique($answersIds))) {
                    throw new HttpException(400, 'Duplicate answer ID.');
                }

                $assignment = $quiz->takers()->find($me->id);

                if ($assignment === null || $assignment->started_at === null || $assignment->finished_at !== null) {
                    throw new AccessDeniedHttpException('Quiz cannot be solved.');
                }

                $answers = $quiz
                    ->answers()
                    ->whereIn('answers.id', $answersIds)
                    ->get(['answers.question_id', 'questions.choice']);

                if ($answers->count() !== count($answersIds)) {
                    throw new HttpException(400, 'Invalid answer ID.');
                }

                $violatedQuestionsCount = $answers
                    ->groupBy('question_id')
                    ->filter(function ($answers) {
                        return ($answers[0]->choice === 'single' && count($answers) >= 2);
                    })
                    ->count();

                if ($violatedQuestionsCount > 0) {
                    throw new HttpException(400, 'Violated question rules.');
                }

                $existingAnswersIds = Answer
                    ::select('answers.id')
                    ->join('answer_user', 'answer_user.answer_id', '=', 'answers.id')
                    ->join('questions', 'questions.id', '=', 'answers.question_id')
                    ->where('answer_user.user_id', $me->id)
                    ->where('questions.quiz_id', $quiz->id)
                    ->lists('id');

                $deleteIds = array_diff($existingAnswersIds, $answersIds);

                if (!empty($deleteIds)) {
                    // checking, because detach([]) deletes everything!
                    $me->answers()->detach($deleteIds);
                }

                $insertIds = array_diff($answersIds, $existingAnswersIds);

                if (!empty($insertIds)) {
                    // checking, because attach([]) throws an error
                    $me->answers()->attach($insertIds);
                }

                $quiz->takers()->updateExistingPivot($me->id, [
                    'finished_at' => DB::raw('NOW()')
                ]);

                $score = $me->countScore($quiz);

                if ($score === false) {
                    $score = null;
                }

                $quiz->takers()->updateExistingPivot($me->id, [
                    'score' => $score
                ]);

                $response = [
                    'success' => true
                ];

                if (Input::has('with')) {
                    $response += $this->attachData(explode(',', Input::get('with')), $me);
                }
            } else {
                $quiz = Quiz
                    ::with([
                        'questions' => function ($query) {
                            $query
                                ->select(
                                    'id',
                                    'quiz_id',
                                    'content',
                                    'hint',
                                    'choice'
                                )
                                ->orderBy('weight')
                                ->orderBy(DB::raw('RAND()'));
                        },
                        'questions.answers' => function ($query) use ($me) {
                            $query
                                ->select(
                                    'answers.id',
                                    'answers.question_id',
                                    'answers.content',
                                    'answers.points',
                                    'answer_user.user_id'
                                )
                                ->leftJoin('answer_user', function ($join) use ($me) {
                                    $join
                                        ->on('answer_user.answer_id', '=', 'answers.id')
                                        ->on('answer_user.user_id', '=', DB::raw($me->id));
                                })
                                ->orderBy('answers.weight')
                                ->orderBy(DB::raw('RAND()'));
                        }
                    ])
                    ->findOrFail($quiz->id, [
                        'id',
                        'title',
                        'description',
                        'icon_file',
                        'icon_background'
                    ]);

                $assignment = $quiz->takers()->find($me->id, ['started_at', 'finished_at']);

                $pivotUpdates = [
                    'started_at' => DB::raw('NOW()')
                ];

                if ($assignment === null || $assignment->started_at === null) {
                    if ($assignment === null) {
                        $quiz->takers()->save($me, $pivotUpdates);
                    } else {
                        $quiz->takers()->updateExistingPivot($me->id, $pivotUpdates);
                    }

                    $assignment = $quiz->takers()->find($me->id, ['started_at', 'finished_at']);
                }

                $quiz = $quiz->toArray() + $assignment->toArray();

                $quiz = array_filter(
                    $quiz,
                    function ($value) {
                        return $value !== null;
                    }
                );

                $quiz['questions'] = array_map(
                    function ($question) use ($assignment) {
                        unset(
                            $question['id'],
                            $question['quiz_id']
                        );

                        $question = array_filter(
                            $question,
                            function ($value) {
                                return $value !== null;
                            }
                        );

                        $question['answers'] = array_map(
                            function ($answer) use ($question, $assignment) {
                                if ($assignment->finished_at !== null) {
                                    if ($answer['points'] > 0) {
                                        $answer['status'] = 'correct';
                                    } elseif ($answer['points'] < 0) {
                                        $answer['status'] = 'wrong';
                                    } else {
                                        $answer['status'] = 'neutral';
                                    }
                                    $answer['selected'] = ($answer['user_id'] !== null);
                                    unset($answer['user_id']);
                                }

                                unset(
                                    $answer['question_id'],
                                    $answer['user_id'],
                                    $answer['points']
                                );

                                return $answer;
                            },
                            $question['answers']
                        );

                        return $question;
                    },
                    $quiz['questions']
                );

                $response = $quiz;
            }

            $status = 200;
        } catch (HttpException $exception) {
            $response = $exception->getMessage();
            $status = $exception->getStatusCode();
        } catch (Exception $exception) {
            $response = config('app.debug') ? $exception->getMessage() : 'Internal error occurred.';
            $status = 500;
        }

        return response()->json($response, $status);
    }
}
