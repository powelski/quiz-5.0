<?php namespace Quiz;

use Illuminate\Database\Eloquent\Model;
use \DB;

class Group extends Model
{
    public function users()
    {
        return $this->belongsToMany('Quiz\User')->withTimestamps();
    }

    public function quizzes()
    {
        return $this
            ->belongsToMany('Quiz\Quiz')
            ->wherePivot('available_at', '<=', DB::raw('NOW()'))
            ->withTimestamps();
    }
}
