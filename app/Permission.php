<?php namespace Quiz;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function users()
    {
        return $this->belongsToMany('Quiz\User');
    }
}
